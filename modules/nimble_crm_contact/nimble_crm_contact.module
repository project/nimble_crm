<?php

/**
 * @file
 * Provides integration of core Contact module with Nimble CRM.
 */

/**
 * Implements hook_menu().
 */
function nimble_crm_contact_menu() {
  $items['admin/structure/contact/nimble-crm'] = array(
    'title' => 'Nimble CRM',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nimble_crm_contact_settings_form'),
    'access arguments' => array('administer contact forms'),
    'type' => MENU_LOCAL_ACTION,
    'weight' => 2,
    'file' => 'nimble_crm_contact.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function nimble_crm_contact_form_contact_site_form_alter(&$form, $form_state) {
  $form['#submit'][] = 'nimble_crm_contact_form_contact_site_form_submit_alter';
}

/**
 * Altered submit callback for contact_site_form().
 */
function nimble_crm_contact_form_contact_site_form_submit_alter(&$form, $form_state) {
  form_state_values_clean($form_state);

  $contact_send = variable_get('nimble_crm_contact_send', 0);
  // Check if contact form is configured to send data the to Nimble CRM.
  if (!empty($contact_send)) {
    // Get contact tags from contact form settings.
    $tags = variable_get('nimble_crm_contact_tags', '');
    // Get processed field values in Nimble CRM format.
    $fields = _nimble_crm_contact_process_fields($form_state['values']);
    nimble_crm_contact_create($fields, $tags);
  }
}

/**
 * Prepares a contact object in Nimble CRM format.
 */
function _nimble_crm_contact_process_fields($values) {
  $fields = new stdClass();

  $field_name = new stdClass();
  $field_name->value = $values['name'];
  $field_name->modifier = '';
  $fields->{'first name'} = array($field_name);

  $field_email = new stdClass();
  $field_email->value = $values['mail'];
  $field_email->modifier = 'other';
  $fields->{'email'} = array($field_email);

  $custom_fields = variable_get('nimble_crm_contact_custom_fields', '');
  if (!empty($custom_fields[0]['name'])) {
    foreach ($custom_fields as $custom_field) {
      $field_custom = new stdClass();
      $field_custom->value = $custom_field['value'];
      $field_custom->modifier = '';
      $fields->{$custom_field['name']} = array($field_custom);
    }
  }
  return $fields;
}
