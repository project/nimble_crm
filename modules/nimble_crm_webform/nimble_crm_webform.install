<?php

/**
 * @file
 * Nimble CRM Webform module install/schema hooks.
 */

/**
 * Implements hook_schema_alter().
 */
function nimble_crm_webform_schema_alter(&$schema) {
  if (isset($schema['webform'])) {
    $schema['webform']['fields']['nimble_crm_send'] = array(
      'description' => 'Boolean value indicating if the results of webform should be send to the Nimble CRM.',
      'type' => 'int',
      'size' => 'tiny',
      'not null' => TRUE,
      'default' => 0,
    );
    $schema['webform']['fields']['nimble_crm_tags'] = array(
      'description' => 'Comma separated list of tags that will be assigned to contact in the Nimble CRM.',
      'type' => 'varchar',
      'length' => 255,
    );
    $schema['webform']['fields']['nimble_crm_custom_fields'] = array(
      'description' => 'A serialized array of custom field names and its default values',
      'type' => 'blob',
      'not null' => FALSE,
      'size' => 'big',
      'serialize' => TRUE,
    );
  }
}

/**
 * Adds Nimble CRM columns to the webform table.
 */
function nimble_crm_webform_install() {
  $table = 'webform';
  $schema[$table] = array();
  nimble_crm_webform_schema_alter($schema);
  foreach ($schema[$table]['fields'] as $name => $spec) {
    if (!db_field_exists($table, $name)) {
      db_add_field($table, $name, $spec);
    }
  }
  $t = get_t();
  return $t('New webform columns added.');
}

/**
 * Removes Nimble CRM columns from the webform table.
 */
function nimble_crm_webform_uninstall() {
  $table = 'webform';
  $schema[$table] = array();
  nimble_crm_webform_schema_alter($schema);
  foreach ($schema[$table]['fields'] as $name => $spec) {
    if (db_field_exists($table, $name)) {
      db_drop_field($table, $name);
    }
  }
  $t = get_t();
  return $t('Removed Nimble CRM columns.');
}
